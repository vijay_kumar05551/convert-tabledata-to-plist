//
//  ViewController.swift
//  Convert tableData to Plist
//
//  Created by OSX on 16/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var showRecords:NSArray = []
    var showImages:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let showTV:UITableView = UITableView(frame: self.view.frame)
        
        showTV.delegate = self;
        showTV.dataSource = self
        
        showTV.rowHeight = 70
        
        self.view.addSubview(showTV)
        
        // Create headerView for tableview
        let headerView:UIView = UIView()
        headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 44)
        headerView.backgroundColor = UIColor.lightGrayColor()
        showTV.tableHeaderView = headerView
        
        // Here i have get the path of the "Plist"
        
        let path:NSString = NSBundle.mainBundle().pathForResource("SocialNetworking", ofType: "plist")!
        
        let dict:NSDictionary = NSDictionary(contentsOfFile: path as String)!
        
        
        showRecords = dict.objectForKey("showRecords") as! [AnyObject]
        showImages = dict.objectForKey("showImages") as! [AnyObject]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showRecords.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier:NSString = "recordsTVCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        
        cell.textLabel?.text = showRecords.objectAtIndex(indexPath.row) as? String
        cell.imageView?.image = UIImage(named: showImages.objectAtIndex(indexPath.row) as! String)
        
        return cell;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

